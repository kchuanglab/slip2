## Publication

This work has been published in Nature Protocols: http://www.nature.com/nprot/journal/v12/n2/full/nprot.2016.181.html

For detailed experimental setups, please refer to the link above.

## What is this repository for?

SLIP2 is designed for high-throughput imaging. It opens a Micro-manager (https://www.micro-manager.org) GUI and automatically goes through the designated positions in a 96- or 384- multiwell plate format.

## How do I get set up? 

### Set up for first installation
Refer to the website: https://www.micro-manager.org/wiki/Matlab

Make sure you have Micro-manager installed and running before the set up below.

###### Add Java path in MATLAB
* Find all.jar files located in the µManager folder: run `MMsetup_javaclasspath.m` in MATLAB. This script creates `MMjavaclasspath.txt` under the MATLAB default directory.


* Run `edit([prefdir '/MMjavaclasspath.txt'])` in MATLAB to open the `MMjavaclasspath.txt` file in the MATLAB editor. Then run `edit([prefdir '/javaclasspath.txt'])`. Copy all contents of `MMjavaclasspath.txt` to `javaclasspath.txt`.

* Save `javaclasspath.txt`.

* In MATLAB, run `edit(‘librarypath.txt’)`. At the end of the file, add a line with the µManager installation directory, for example, `C:/Program Files/Micro-Manager-1.4`. Save `librarypath.txt`.

* Restart MATLAB. To check whether the path has been updated, run `javaclasspath`, the Micro-manager jar files should appear at the very beginning.

###### Add path for micromanager dll in system
* Right-click 'Computer' on the desktop or start menu.
* Click 'properties --> Advanced system settings', hilight the 'Advance' tab, and click 'Environment Variables...'
* On the 'System variables' list, find 'Path' and click 'Edit'. DO NOT DELETE ANYTHING. At the end, add `;C:/Program Files/Micro-Manager-1.4` or the appropriate Micromanager installation directory (do not omit the leading semicolon).
* Save the changes and restart the computer to make the change of path effective.


###### Dual camera setup (optional)
Useful information: https://micro-manager.org/wiki/Andor#Dual_camera_setup

Below is a quick summary of the above webpage.

* Make sure both cameras are connected and recognized by the computer (use Device Manager in computer).
* Open Micromanager. Click ‘Tool → Device Property Browser’, and find the fields ‘Multi Camera-Physical Camera 1’ and ‘Multi Camera-Physical Camera 2’. Set their values to be the two cameras installed. Assign the slave camera to ‘Multi Camera-Physical Camera 1’, and the master camera to ‘Multi Camera-Physical Camera 2’.
* Set 'core-camera' to 'multi-camera' and test.


###### Connect TTL
* SLIP2 creates triggering between camera and stage to allow fast image acquisition.
* Use BNC or other compatible cables to connect the camera and the stage controller.
* Connect camera 'arm' to stage 'input' (designates when camera is ready for next exposure, signals stage to begin movement when camera is done exposing).
* Connect camera 'external triggering' to stage 'output' (TTL signals from stage signal camera exposure).
* (Optional) Connect camera 'fire' to LED controller.
* (Optional) For dual camera setup, connect the master camera as described above;


### Set up for each experiment run
* Start SLIP by running SLIP.m in MATLAB. This script will open the µManager GUI. Choose the appropriate µManager configuration file, and the program will initialize.

* Click ‘Calibrate’ in the SLIP window, and SLIP will display the calibration menu. After specifying the strain locations for calibration, use the “Live” imaging feature in µManager to locate the center of these strain positions. The positions of all strains on the plate will be calculated based on the calibration.

* Set the desired imaging parameters (plate layout, strains to image, number of images for each strain, exposure time, etc.). Typically, imaging a 5x5 grid (25 images in total) for each strain yields data from enough cells for the statistical analysis of interest, and field by field variation from the same well is ~3%. The number of images should be adjusted based on cell densities. Since image acquisition is only a small fraction of the total time required for SLIP, the imaging grid size can be selected based on the strain(s) with the lowest density without substantially affecting the SLIP time. 

* For time-lapse imaging, set the desired imaging interval and total time points. Typically, a full 96-well plate requires at least ~4 min to image one time point, and acquisition time scales linearly with the number of images acquired for each well; the defined interval should be longer than the total acquisition time for one time point.

* In the SLIP window, check that ‘Pause on PFS loss’ is enabled.

* Select channels for imaging in the selection box. Note that for two or more channels without a dual camera setup, image acquisition will take significantly longer.

* Once all parameters are set, start image acquisition. Fix the focus when necessary.


## Contact
For problems on the software, please create issues on the bitbucket project page: https://bitbucket.org/kchuanglab/slip2