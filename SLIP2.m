function varargout = SLIP2(varargin)
% SLIP2 MATLAB code for SLIP2.fig
%      SLIP2, by itself, creates a new SLIP2 or raises the existing
%      singleton*.
%
%      H = SLIP2 returns the handle to a new SLIP2 or the handle to
%      the existing singleton*.
%
%      SLIP2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SLIP2.M with the given input arguments.
%
%      SLIP2('Property','Value',...) creates a new SLIP2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SLIP2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SLIP2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES
% Edit the above text to modify the response to help SLIP2

% Last Modified by GUIDE v2.5 18-Sep-2016 13:39:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SLIP2_OpeningFcn, ...
                   'gui_OutputFcn',  @SLIP2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%% functions for strain definitions
% --- Executes on selection change in popup_platelayout.
function popup_platelayout_Callback(hObject, eventdata, handles)
% hObject    handle to popup_platelayout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_platelayout contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_platelayout
drawplate(handles,handles.axes_plate);

% --- Executes during object creation, after setting all properties.
function popup_platelayout_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_platelayout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in push_selectall.
function push_selectall_Callback(hObject, eventdata, handles)
% hObject    handle to push_selectall (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global oncolor

circledata = get(handles.axes_plate,'UserData');
circleh = circledata{1};
for hh = circleh(:)'
    set(hh,'FaceColor',oncolor);
end

% --- Executes on button press in push_selectnone.
function push_selectnone_Callback(hObject, eventdata, handles)
% hObject    handle to push_selectnone (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global offcolor

circledata = get(handles.axes_plate,'UserData');
circleh = circledata{1};
for hh = circleh(:)'
    set(hh,'FaceColor',offcolor);
end

% --- Executes on button press in push_toggle.
function push_toggle_Callback(hObject, eventdata, handles)
% hObject    handle to push_toggle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
circledata = get(handles.axes_plate,'UserData');
circleh = circledata{1};
for hh = circleh(:)'
    togglePosition(hh);
end

% --- Executes on button press in push_manual.
function push_manual_Callback(hObject, eventdata, handles)
% hObject    handle to push_manual (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global basedim
global mf
axes(handles.axes_plate);
currect = getrect;
currect(3:4) = currect(1:2)+currect(3:4);
circledata = get(handles.axes_plate,'UserData');
circleh = circledata{1};
circlepos = circledata{2};
tochange = find((circlepos(:,1)>currect(1)).*(circlepos(:,1)<currect(3)).*(circlepos(:,2)>currect(2)).*(circlepos(:,2)<currect(4)));
for l = 1:length(tochange)
    [j,k] = ind2sub(mf*basedim',tochange(l));
    curcirc = circleh(j+(k-1)*basedim(1)*mf);
    togglePosition(curcirc);
end
    
% --- Executes on button press in push_calibrate.
function push_calibrate_Callback(hObject, eventdata, handles)
% hObject    handle to push_calibrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global Scp
% Scp.mmc.setOriginXY(Scp.mmc.getXYStageDevice)
% the two lines commented out for further development of calibration
global Scp
global letters
global basedim
global mf
global wellsep
global dtheta
% prompt to request hitting reset every calibration
uiwait(msgbox('Press the reset button at the back of the XY stage. Click OK after reset is done.'));
numwells2cal = questdlg('How many wells do you want to calibrate?', ...
    'Calibration option', ...
    '1', '2', '2');
switch numwells2cal
    case '1'
        wellind1 = choosecaliwell(1, 1, 'Choose well for calibration');
        uiwait(msgbox(['Move to Well ', letters(wellind1(1)), num2str(wellind1(2)), ' and press OK.']));
        pos1 = Scp.mmc.getXYStagePosition(Scp.mmc.getXYStageDevice);
        x1 = pos1.getX();
        y1 = pos1.getY();
        [A1x, A1y] = calcCaliPara1(wellind1, [x1, y1]);
        Scp.mmc.setXYPosition(Scp.mmc.getXYStageDevice, A1x, A1y);
        scopebusy();
        Scp.mmc.setOriginXY(Scp.mmc.getXYStageDevice);
        dtheta = 0;
    case '2'
        wellind1 = choosecaliwell(1, 1, 'Choose 1st well for calibration');
        uiwait(msgbox(['Move to Well ', letters(wellind1(1)), num2str(wellind1(2)), ' and press OK.']));
        pos1 = Scp.mmc.getXYStagePosition(Scp.mmc.getXYStageDevice);
        x1 = pos1.getX();
        y1 = pos1.getY();
        wellind2 = choosecaliwell(basedim(1) * mf, basedim(2) * mf, 'Choose 2nd well for calibration');
        Scp.mmc.setRelativeXYPosition(Scp.mmc.getXYStageDevice, wellsep * (wellind2(2) - wellind1(2)) / mf, ...
            wellsep * (wellind2(1) - wellind1(1)) / mf); % x moves in the negative position...
        scopebusy();
        uiwait(msgbox(['Fine tune the position for Well ', letters(wellind2(1)), num2str(wellind2(2)), ' and press OK.']));
        pos2 = Scp.mmc.getXYStagePosition(Scp.mmc.getXYStageDevice);
        x2 = pos2.getX();
        y2 = pos2.getY();
        [dtheta, A1x, A1y] = calcCaliPara2(wellind1, [x1, y1], wellind2, [x2, y2]);
        Scp.mmc.setXYPosition(Scp.mmc.getXYStageDevice, A1x, A1y);
        scopebusy();
        Scp.mmc.setOriginXY(Scp.mmc.getXYStageDevice);
end
uiwait(msgbox('Calibration finished.'));
% enable "run" and "goto well" buttons only after calibration
set(handles.pushbutton_gotowell, 'enable', 'on');
set(handles.push_run, 'enable', 'on');
        

% --- Executes on button press in radio_scopeview.
function radio_scopeview_Callback(hObject, eventdata, handles)
% hObject    handle to radio_scopeview (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radio_scopeview
% handles    structure with handles and user data (see GUIDATA)
set(handles.radio_scopeview,'Value',1)
set(handles.radio_topview,'Value',0)
drawplate(handles,handles.axes_plate);

% --- Executes on button press in radio_topview.
function radio_topview_Callback(hObject, eventdata, handles)
% hObject    handle to radio_topview (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radio_topview
set(handles.radio_scopeview,'Value',0)
set(handles.radio_topview,'Value',1)
drawplate(handles,handles.axes_plate);

% --- Executes on mouse press over axes background.
function axes_plate_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes_plate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

circledata = get(handles.axes_plate,'UserData');
circleh = circledata{1};
circlepos = circledata{2};

curpoint = get(hObject,'CurrentPoint');
curpoint = curpoint(1,1:2);

[~,pos] = min(pdist2(curpoint,circlepos));
togglePosition(circleh(pos(1)));

% --- currently empty
function edit_metadata_Callback(hObject, eventdata, handles)
% hObject    handle to edit_metadata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_metadata as text
%        str2double(get(hObject,'String')) returns contents of edit_metadata as a double

% --- Executes during object creation, after setting all properties.
function edit_metadata_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_metadata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% functions for file options
% --- Executes on button press in radio_namebyfile.
function radio_namebyfile_Callback(hObject, eventdata, handles)
% hObject    handle to radio_namebyfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radio_namebyfile
set(handles.radio_autoname,'Value',0)
set(handles.radio_namebyfile,'Value',1)
set(handles.push_loadfile,'Enable','on');

% --- Executes on button press in push_loadfile.
function push_loadfile_Callback(hObject, eventdata, handles)
% hObject    handle to push_loadfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Scp
[file2read,folder2read] = uigetfile('*.xlsx');
if ischar(file2read)
    [~,~,rawnames] = xlsread([folder2read,file2read]);
    Scp.names = rawnames;
else
    Scp.names = [];
end

% --- Executes on button press in radio_autoname.
function radio_autoname_Callback(hObject, eventdata, handles)
% hObject    handle to radio_autoname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radio_autoname
set(handles.radio_autoname,'Value',1)
set(handles.radio_namebyfile,'Value',0)
set(handles.push_loadfile,'Enable','off');

% should not be necessary since the radios are each defined -- Handuo Shi
% 12/01/2015
% % --- Executes when selected object is changed in uipanel6.
% function uipanel6_SelectionChangeFcn(hObject, eventdata, handles)
% % hObject    handle to the selected object in uipanel6 
% % eventdata  structure with the following fields (see UIBUTTONGROUP)
% %	EventName: string 'SelectionChanged' (read only)
% %	OldValue: handle of the previously selected object or empty if none was selected
% %	NewValue: handle of the currently selected object
% % handles    structure with handles and user data (see GUIDATA)
% if get(handles.radio_namebyfile,'Value')
%     set(handles.push_loadfile,'Enable','On');
% else
%     set(handles.push_loadfile,'Enable','Off');
% end

function edit_acq_Callback(hObject, eventdata, handles)
% hObject    handle to edit_acq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_acq as text
%        str2double(get(hObject,'String')) returns contents of edit_acq as a double

% --- Executes during object creation, after setting all properties.
function edit_acq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_acq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in push_rootdir.
function push_rootdir_Callback(hObject, eventdata, handles)
% hObject    handle to push_rootdir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Scp
curdir = uigetdir(Scp.rootDirName);
if ~isempty(curdir)
    Scp.rootDirName = curdir;
    set(handles.text_savedir,'String',Scp.rootDirName);
end

%% functions for imaging options -- accepts inputs and set exposure
function edit_fovrows_Callback(hObject, eventdata, handles)
% removed varargout since not being used -- Handuo Shi 12/01/2015
% function varargout = edit_fovrows_Callback(hObject, eventdata, handles)
% hObject    handle to edit_fovrows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_fovrows as text
%        str2double(get(hObject,'String')) returns contents of edit_fovrows as a double
updateFOVgrid(handles);

% --- Executes during object creation, after setting all properties.
function edit_fovrows_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_fovrows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_fovcols_Callback(hObject, eventdata, handles)
% hObject    handle to edit_fovcols (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_fovcols as text
%        str2double(get(hObject,'String')) returns contents of edit_fovcols as a double
updateFOVgrid(handles);

% --- Executes during object creation, after setting all properties.
function edit_fovcols_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_fovcols (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_exposure_Callback(hObject, eventdata, handles)
% hObject    handle to edit_exposure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_exposure as text
%        str2double(get(hObject,'String')) returns contents of edit_exposure as a double
global Scp
curexp = str2double(get(hObject,'String'));
cameraproperties = {{'Exposure', curexp}}; % added in actual exposure time -- Handuo Shi 12/01/2015, tested working 12/02/2015
cellfun(@(x) setDeviceProperty(char(Scp.mmc.getCameraDevice), x{1},x{2}),cameraproperties);

% --- Executes during object creation, after setting all properties.
function edit_exposure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_exposure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% functions for timelapse options -- basically no action but to accept changes and save in handles
function edit_timepoints_Callback(hObject, eventdata, handles)
% hObject    handle to edit_timepoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_timepoints as text
%        str2double(get(hObject,'String')) returns contents of edit_timepoints as a double

% --- Executes during object creation, after setting all properties.
function edit_timepoints_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_timepoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_timeinterval_Callback(hObject, eventdata, handles)
% hObject    handle to edit_timeinterval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_timeinterval as text
%        str2double(get(hObject,'String')) returns contents of edit_timeinterval as a double

% --- Executes during object creation, after setting all properties.
function edit_timeinterval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_timeinterval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% functions for focus options
% --- Executes on button press in check_autofocusall.
function check_autofocusall_Callback(hObject, eventdata, handles)
% hObject    handle to check_autofocusall (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_autofocusall
if get(hObject,'Value')
   set(handles.check_autofocusfirst,'Value',0);
end

% --- Executes on button press in check_autofocusfirst.
function check_autofocusfirst_Callback(hObject, eventdata, handles)
% hObject    handle to check_autofocusfirst (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_autofocusfirst
if get(hObject,'Value')
   set(handles.check_autofocusall,'Value',0);
end

% --- Executes on button press in check_pfsloss.
function check_pfsloss_Callback(hObject, eventdata, handles)
% hObject    handle to check_pfsloss (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_pfsloss

%% functions for experiment control

% --- Executes on button press in push_stop.
function push_stop_Callback(hObject, eventdata, handles)
% hObject    handle to push_stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global stop
stop = 1;
set(hObject,'UserData',1);
guidata(hObject,handles)

% --- Executes on button press in push_reset.
function push_reset_Callback(hObject, eventdata, handles)
% hObject    handle to push_reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global Scp
% Scp.gui.closeAcquisition(Scp.acqName);
% triggerInternal();
% lines below added for cleaner reset -- Handuo Shi 12/01/2015
% clearvars
% close all
global Scp
Scp.gui.closeAllAcquisitions();
pause(0.1)
SLIP2

% --- Executes on selection change in channel_listbox.
function channel_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to channel_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns channel_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from channel_listbox


% --- Executes during object creation, after setting all properties.
function channel_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to channel_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_gotowell.
function pushbutton_gotowell_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_gotowell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% move the stage to a certain well
global Scp

wellind = choosecaliwell(1, 1, 'Go to well...');
[posx,posy] = dotcenter_corrected(wellind(2), wellind(1));
stagename = Scp.mmc.getXYStageDevice();
Scp.mmc.setXYPosition(stagename,posx,posy);
scopebusy();
