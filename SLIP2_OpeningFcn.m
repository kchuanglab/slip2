% --- Executes just before SLIP2 is made visible.
function SLIP2_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to SLIP2 (see VARARGIN)

    % Choose default command line output for SLIP2
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

    % Initialize g 
    import mmcorj.*;
    import org.micromanager.*;
    import org.micromanager.MMStudio; % added per current MM wiki 02/05/2018 HS
    import org.micromanager.api.MultiStagePosition;
    import org.micromanager.api.StagePosition;
    import org.micromanager.api.PositionList;
    import org.micromanager.utils.MDUtils;

    %% Set up Micromanager
    global Scpname;
    Scpname = questdlg('Which microscope are you using?', ...
            'Microscope option', ...
            'colossus', 'vulcan', 'polaris', 'others');
    global DualCam;
    if strcmp(Scpname, 'polaris')
        dualchoice = questdlg('Are you using twin-camera today?', ...
            'Twin camera option', ...
            'Yes', 'No', 'No');
        switch dualchoice
            case 'Yes'
                DualCam = 1;
            case 'No'
                DualCam = 0;
        end
    else
        DualCam = 0;
    end
    global Scp;
    Scp = struct([]);
%     Scp(1).studio = MMStudioPlugin;
%     Scp.studio.run('');
    Scp(1).gui = MMStudio(false);
    uiwait(msgbox('Press OK when Micro-manager has finished loading.'));
%     Scp.mmc = Scp.studio.getMMCoreInstance;
%     Scp.gui = Scp.studio.getMMStudioInstance;
%     Scp.acq = Scp.gui.getAcquisitionEngine;
    Scp.mmc = Scp.gui.getCore;
    Scp.acq = Scp.gui.getAcquisitionEngine;
    Scp.mdutils = MDUtils;
    Scp.MultiStagePosition = MultiStagePosition;
    Scp.rootDirName = 'D:/tmp/'; % empty = not actually saving.
    Scp.channelGroup = 'Channel';
    Scp.channel = 'Phase';
    % clear all ealier stuff
    Scp.gui.closeAllAcquisitions();
    Scp.gui.clearMessageWindow();
    Scp.micronsperpixel = .0644; % microns per pixel (%updated when run is hit)
    Scp.overlap = .95; % how much you want images to overlap, in percent of image dimensions
    % stage stuff
    Scp.stage = struct([]);
    Scp.stage(1).device = Scp.mmc.getXYStageDevice;
    % Name stuff
    Scp.names = [];
    %Scp.stage.orix = Scp.mmc.getXPosition(Scp.stage.device);
    %Scp.stage.oriy = Scp.mmc.getYPosition(Scp.stage.device);
    %Scp.stage.posx = Scp.mmc.getXPosition(Scp.stage.device);
    %Scp.stage.posy = Scp.mmc.getYPosition(Scp.stage.device);
    %Scp.mmc.setOriginXY(Scp.mmc.getXYStageDevice);

    % microscope-specific stuff to make it faassstttt
    % property - value pairs for core.
    coreproperties = {{'TimeoutMs',1500}}; % changed from 30000 to 1500 for debugging

    % property - value pairs for camera

    % set properties based on scopes
    switch Scpname
        case 'colossus'
            cameraproperties = {{'Overlap','On'},...
                {'PixelReadoutRate','560 Mhz'}, ...
                {'Sensitivity/DynamicRange','11-bit (low noise)'},...
                {'TriggerMode','External'},...
                {'Exposure',100},...
                {'Ext (Exp)',10000}};
        case 'vulcan'
            cameraproperties = {{'FrameTransfer','On'},...
                {'PixelType','16bit'},...
                {'Trigger','External'},...
                {'Exposure',100}};
        case 'polaris'
            cameraproperties = {{'Overlap','On'}, ...
                {'PixelReadoutRate','560 Mhz'}, ...
                {'Sensitivity/DynamicRange','12-bit (low noise)'},...
                {'TriggerMode','External'},...
                {'Exposure',100},...
                {'Ext (Exp)',10000}};
            cameranames = {'Zyla 1', 'Zyla 2'};
            if ~DualCam
                Scp.mmc.setCameraDevice('Zyla 2');
            end
        otherwise
            error('Scope name not recognized!');
    end

    % property - value pairs for stage
    stageproperties = {{'Speed-S',6.5},...
        {'Acceleration',30},...
        {'Backlash',0}};%,...
    %     
    cellfun(@(x) setDeviceProperty('Core', x{1},x{2}),coreproperties);
    if DualCam
        Scp.mmc.setCameraDevice('Zyla 1');
        cellfun(@(x) setDeviceProperty(char(Scp.mmc.getCameraDevice), x{1},x{2}),cameraproperties);
        Scp.mmc.setCameraDevice('Zyla 2');
        cellfun(@(x) setDeviceProperty(char(Scp.mmc.getCameraDevice), x{1},x{2}),cameraproperties);
        Scp.mmc.setCameraDevice('Multi Camera');
    else
        cellfun(@(x) setDeviceProperty(char(Scp.mmc.getCameraDevice), x{1},x{2}),cameraproperties);
    end
    cellfun(@(x) setDeviceProperty(char(Scp.mmc.getXYStageDevice), x{1},x{2}),stageproperties);

    % saving stuff
    set(handles.text_savedir,'String',Scp.rootDirName);
    Scp.imageWidth = Scp.mmc.getImageWidth();
    Scp.imageHeight = Scp.mmc.getImageHeight();
    Scp.bytesPerPixel = Scp.mmc.getBytesPerPixel();
    Scp.bitDepth = Scp.mmc.getImageBitDepth();

    % stop functionality
    set(handles.push_stop,'UserData',0);
%%
    % Make plate
    global basedim
    basedim = [8,12]; % 96-well plate.
    global letters
    letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; % for translation row to letter
    global wellsep
    wellsep = 9000; %% microns between wells in a 96-well plate.
    global mf
    mf = 1; % multiplication-factor between 96/384/1536 well plates

    h = handles.axes_plate;

    drawplate(handles,h);
    % Make internal trigger.
    triggerInternal();

    % UIWAIT makes SLIP2 wait for user response (see UIRESUME)
    % uiwait(handles.figure1);