function [z,s]=SLIPautofocus(Scp,AcqData)
%             persistent XY;
z=nan;
s=nan;
switch lower(Scp.AutoFocusType)
    case 'none'
        disp('No autofocus used');
    case 'hardware'
        %                     currXY=Scp.XY;
        %                     if isempty(XY)
        %                         XY = currXY;
        %                     elseif sqrt((XY(1)-currXY(1))^2+(XY(2)-currXY(2))^2)<100
        %                         return;
        %                     end
        Scp.mmc.enableContinuousFocus(true);
        t0=now;
        while ~Scp.mmc.isContinuousFocusLocked && (now-t0)*24*3600 < Scp.autofocusTimeout
            pause(Scp.autofocusTimeout/1000)
        end
        if (now-t0)*24*3600 > Scp.autofocusTimeout
            msgbox('Out of Focus ! watch out!!!');
        end
    case 'software'
        if length(AcqData)~=1
            error('autofocus can only get a single channel!')
        end
        if isfield(AcqData,'ROI')
            roi=AcqData.ROI;
        end
        % set up imaging parameters
        if ~strcmp(Scp.Channel,AcqData.Channel)
            Scp.Channel=AcqData.Channel;
        end
        Scp.Exposure=AcqData.Exposure;
        
        % set up binning
        if isfield(AcqData,'binning')
            Scp.mmc.setProperty('CoolSNAP','Binning',num2str(AcqData.binning));
            if isfield(AcqData,'ROI')
                roi=ceil(roi./AcqData.binning);
            end
        end
        % set up ROI [x top left, y top left, width, height]
        if isfield(AcqData,'ROI')
            Scp.mmc.setROI(roi(1),roi(2),roi(3),roi(4));
        end
        w=Scp.Width;
        h=Scp.Height;
        bd=Scp.BitDepth;
        
        % define image anlysis parameters
        hx = fspecial('sobel');
        hy= fspecial('sobel')';
        
        % Z to test on
        Z0=Scp.Z;
        Zv=Z0+linspace(-6,6,Scp.autogrid);
        S=zeros(size(Zv));
        Tlog=zeros(size(Zv));
        
        % open shutter
        Scp.mmc.setAutoShutter(0)
        Scp.mmc.setShutterOpen(1)
        
        % run a Z scan
        for i=1:length(Zv)
            tic
            Scp.Z=Zv(i);
            Scp.mmc.snapImage;
            imgtmp=Scp.mmc.getImage;
            img=reshape(single(imgtmp)./(2^bd),w,h)';
            gx=imfilter(img,hx);
            gy=imfilter(img,hy);
            S(i)=mean(hypot(gx(:),gy(:)));
            Tlog(i)=now;
        end
        % close shutter
        Scp.mmc.setShutterOpen(0)
        Scp.mmc.setAutoShutter(1)
        
        f=@(p,x) exp(-(x-p(1)).^2./2/p(2).^2);
        p0=[Z0 1.5];
        opt=optimset('Display','off');
        p=lsqcurvefit(f,p0,Zv,mat2gray(S),[min(Zv) 0.5],[max(Zv) 2.5],opt);
        z=p(1);
        s=interp1(Zv,S,z);
        
        % clear ROI if it was used
        Scp.mmc.clearROI;
        
        Scp.AFscr=S;
        Scp.AFgrid=Zv;
        
        % move to the new Z
        Scp.Z=z;
        
        
        % update the log
        Scp.AFlog.Z=[Scp.AFlog.Z(:); Zv(:)];
        Scp.AFlog.Time=[Scp.AFlog.Time(:); Tlog(:)];
        Scp.AFlog.Score=[Scp.AFlog.Score(:); S(:)];
        Scp.AFlog.Type=[Scp.AFlog.Type; repmat({'Scan'},length(Zv),1)];
        Scp.AFlog.Channel=[Scp.AFlog.Channel; repmat({AcqData.Channel},length(Zv),1)];
    otherwise
        error('Please define type of autofocus (as None if none exist)')
end

end