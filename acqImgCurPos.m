function acqImgCurPos(handles, platecol, platerow, isfocus, timepos, isext, channel_list, pos_list, exp_list, sendmail, mailaddress)
    % image acquisition for current position
    global Scp
    global mf
    global basedim
    global letters
    global DualCam
    global stop
    
    
    % Get fov information
    [nrows,ncols] = updateFOVgrid(handles);
    numFrames = nrows*ncols;
    
    % get position -- corrected for rotation, edited by Handuo Shi
    % 01/29/2016
    [posx,posy] = dotcenter_corrected(platecol, platerow);
    
    % editing based on Anthony's request 03/17/2017 -- dry tested
    Scp.imageWidth = Scp.mmc.getImageWidth();
    Scp.imageHeight = Scp.mmc.getImageHeight();
    Scp.micronsperpixel = Scp.mmc.getPixelSizeUm();
    mvx = Scp.overlap*Scp.micronsperpixel*Scp.imageWidth/1000;
    mvy = Scp.overlap*Scp.micronsperpixel*Scp.imageHeight/1000;
    [nrows,ncols] = updateFOVgrid(handles);
    posx = posx - mvx * ncols / 2;
    posy = posy - mvy * nrows / 2;

    % naming:
    if ~isempty(Scp.names)
        curname = Scp.names{platerow,platecol};
    else
        curname = [letters(platerow),num2str(platecol)];
    end
    curpos = (platerow-1)*mf*basedim(2)+platecol;


    % move to current position
    if isext
        moveStage(posx,posy);
    else
        stagename = Scp.mmc.getXYStageDevice();
        Scp.mmc.setXYPosition(stagename,posx,posy);
        scopebusy();
    end
    pause(.1);
    
    % Do autofocus
    if isfocus
        autofocus();
    end

    % check PFS loss
    if get(handles.check_pfsloss,'Value')
        checkpfsloss(isext, sendmail, mailaddress);
    end
    
    if isext
        if ~stop
            Scp.mmc.setConfig('Channel', channel_list{1});
            Scp.mmc.setExposure(exp_list(1));

            % start imaging:
            Scp.mmc.startSequenceAcquisition(numFrames, 0, false);
            Scp.mmc.setProperty('XYStage','SerialCommand','RM X=1');    

            % retrieve images
            imagepos = 0;
            while Scp.mmc.isSequenceRunning() || Scp.mmc.getRemainingImageCount() > 0

                if Scp.mmc.getRemainingImageCount() > 0   
                    img = Scp.mmc.popNextTaggedImage();
                    Scp.mdutils.setPositionName(img.tags, curname);
                    Scp.gui.displayImage(img.pix);
                    Scp.gui.addImageToAcquisition(Scp.acqName, timepos, 0, imagepos, curpos, img);
                    imagepos = imagepos + 1;
                    if DualCam
                        img = Scp.mmc.popNextTaggedImage();
                        Scp.mdutils.setPositionName(img.tags, curname);
                        Scp.gui.addImageToAcquisition(Scp.acqName, timepos, 1, imagepos, curpos, img);
                    end
                    imagepos = imagepos + 1;
                end
            end
            Scp.mmc.stopSequenceAcquisition();
        end
    else
        for imagepos = 1 : size(pos_list, 2)
            if ~stop
                if imagepos > 1
                    Scp.mmc.setRelativeXYPosition(stagename, ...
                        (pos_list(1, imagepos) - pos_list(1, imagepos - 1)), ...
                        (pos_list(2, imagepos) - pos_list(2, imagepos - 1)));
                    disp((pos_list(1, imagepos) - pos_list(1, imagepos - 1)));
                    disp((pos_list(2, imagepos) - pos_list(2, imagepos - 1)));
                    scopebusy();
                end
                % image through multiple channels
                for curchannelind = 1 : length(channel_list)
                    channelname = channel_list{curchannelind};
                    Scp.mmc.setExposure(exp_list(curchannelind));
                    Scp.mmc.setConfig('Channel', channelname);
                    Scp.mmc.waitForConfig('Channel', channelname);
                    scopebusy();
                    snapAndAddImage(Scp.acqName, timepos - 1, curchannelind - 1, imagepos - 1, curpos - 1, curname); % this is faster than using the block above -- 07/13/2016 tested
                    Scp.mmc.stopSequenceAcquisition();
                end
            end
        end
    end