function autofocus()
global Scp
% global stop -- not used in function Handuo Shi 12/01/2015


triggerInternal();

focusdevice = 'TIPFSOffset';

% go around current offset
curpfs = Scp.mmc.getProperty(focusdevice,'Position');
totest = str2double(curpfs) + linspace(-60,60,15);
totest = totest(totest>0);

hx = fspecial('sobel');
S = zeros(size(totest));
for aa = 1:length(totest)
    Scp.mmc.setProperty(focusdevice,'Position',round(totest(aa)));
    Scp.mmc.waitForDevice(focusdevice)
    Scp.mmc.snapImage;
    imgtmp = Scp.mmc.getImage;
    Scp.gui.displayImage(imgtmp);
    imgA = reshape(single(imgtmp)./(2^Scp.bitDepth),Scp.imageWidth,Scp.imageHeight)';
    gx = imfilter(imgA,hx);
    gy = imfilter(imgA,hx);
    S(aa) = mean(hypot(gx(:),gy(:)));
end
% move to best offset
[~,imax] = max(S);
Scp.mmc.setProperty(focusdevice,'Position',totest(imax));
Scp.mmc.waitForDevice(focusdevice)

triggerExternal();
