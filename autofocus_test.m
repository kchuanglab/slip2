curpos = Scp.mmc.getProperty('TIPFSOffset','Position');
hx = fspecial('sobel');
hy = fspecial('sobel');
totest = str2double(curpos) + linspace(-60,60,15);
S = zeros(size(totest));
Tlog = zeros(size(totest));
for i = 1:length(totest)
tic
Scp.mmc.setProperty('TIPFSOffset','Position',round(totest(i)));
Scp.mmc.waitForDevice('TIPFSOffset')
Scp.mmc.snapImage;
imgtmp = Scp.mmc.getImage;
Scp.gui.displayImage(imgtmp);

img = reshape(single(imgtmp)./(2^Scp.bitDepth),Scp.imageWidth,Scp.imageHeight)';
gx = imfilter(img,hx);
gy = imfilter(img,hy);
S(i) = mean(hypot(gx(:),gy(:)));
Tlog(i) = now;
end
%%
Scp.mmc.setProperty('TIPFSOffset','Position',100);
for l = 1:100
    disp(Scp.mmc.getProperty('TIPFSOffset','Position'))
end