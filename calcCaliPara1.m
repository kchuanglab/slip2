function [A1x, A1y] = calcCaliPara1(wellind1, pos1)
    
    global mf
    global wellsep
    
    r1 = wellind1(1);
    c1 = wellind1(2);
    
    x1 = pos1(1);
    y1 = pos1(2);
    
    A1x = x1 - (c1 - 1) * wellsep / mf;
    A1y = y1 - (r1 - 1) * wellsep / mf;