function [dtheta, A1x, A1y] = calcCaliPara2(wellind1, pos1, wellind2, pos2)
% takes the input for two well indeces and actual positions, estimate the
% rotation and translation of a plate

    global mf
    global wellsep
    
    r1 = wellind1(1);
    c1 = wellind1(2);
    r2 = wellind2(1);
    c2 = wellind2(2);
    
    x1 = pos1(1);
    y1 = pos1(2);
    x2 = pos2(1);
    y2 = pos2(2);
    
    dx0 = (c2 - c1) * wellsep / mf;
    dy0 = (r2 - r1) * wellsep / mf;
    theta0 = atan(dy0 / dx0);
    
    dx = x2 - x1;
    dy = y2 - y1;
    theta = atan(dy / dx);
    
    dtheta = theta0 - theta;
    
    xc = (x1 + x2) / 2;
    yc = (y1 + y2) / 2;
    
    rc = (r1 + r2) / 2 - 1;
    cc = (c1 + c2) / 2 - 1;
    
    theta1 = atan(cc / rc);
    
    D = wellsep / mf * sqrt(rc ^ 2 + cc ^ 2);
    
    A1x = xc - D * sin(theta1 + dtheta);
    A1y = yc - D * cos(theta1 + dtheta);