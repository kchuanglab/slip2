function checkpfsloss(isext, sendmail, mailaddress)

    global Scp
    pfs = Scp.mmc.getAutoFocusDevice;
    stagename = Scp.mmc.getXYStageDevice();
    if strcmp(Scp.mmc.getProperty(pfs,'Status'),'Focus lock failed')
        if isext
            Scp.mmc.setProperty(stagename,'SerialCommand','TTL Y=0');
            uiwait(msgbox('Fix PFS and press OK'));
            Scp.gui.enableLiveMode(false); % to make sure live mode is off
            Scp.mmc.setProperty(stagename,'SerialCommand','TTL Y=2');
            Scp.mmc.setProperty(stagename,'SerialCommand','AHOME');
        else
            if sendmail
                try
                matlabmail(mailaddress, '', 'SLIP has lost focus');
                end
            end
            uiwait(msgbox('Fix PFS and press OK'));
            Scp.gui.enableLiveMode(false); % to make sure live mode is off
        end
    end
    end