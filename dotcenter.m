function [curcentx, curcenty] = dotcenter(curcol,currow)
% silly formulas for calculating dot center.
% It's in a dedicated function since multiple functions call it.
% so edit this formula if you want to chance dot positioning.

% global basedim
global wellsep
global mf

% y position is simply a function which row its on
curcenty = (currow-1)*wellsep/mf;
% x position goes left, so negative
curcentx = -(curcol-1)*wellsep/mf;