function [realcentx, realcenty] = dotcenter_corrected(platecol, platerow)
% calculate the corrected xy positions

    global wellsep
    global mf
    global dtheta
    
    % y position is simply a function which row its on
    curcenty = (platerow - 1) * wellsep / mf;
    % x position goes left, so negative -- will need to test, not negative
    % for now 01/29/2016
    curcentx = (platecol - 1) * wellsep / mf;
    
    realcentx = curcentx * cos(dtheta) + curcenty * sin(dtheta);
    realcenty = curcenty * cos(dtheta) - curcentx * sin(dtheta);