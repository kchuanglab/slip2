function drawplate(handles,h)
    global basedim
    global letters
    global mf % added by Handuo Shi, 12/01/2015
    % Check that number of plates is 96-well like.
    curplate = get(handles.popup_platelayout,'Value');
    platelist = get(handles.popup_platelayout,'UserData');
    numstrains = platelist(curplate);
    mf = log(numstrains/basedim(1)/basedim(2))/log(2);
    if mf==0
        mf = 1;
    end
    if round(mf)~=mf
        error('That''s not a 96X-like plate!');
    end
    cla(h);
    xaxislabel = 1:(basedim(2)*mf);
    yaxislabel = (basedim(1)*mf):-1:1;

    % top view or scope view?
    scopeview = get(handles.radio_scopeview,'Value');
    if scopeview
        xaxislabel = fliplr(xaxislabel);   
    end
    % all this does is flip x-axis labels.

    % render:
    axis equal;
    set(h,'XTick',sort(xaxislabel),'YTick',sort(yaxislabel),'XTickLabel',xaxislabel,'YTickLabel',arrayfun(@(x) x,letters(yaxislabel),'UniformOutput',0));
    set(h,'XLim',[0,basedim(2)*mf+1],'YLim',[0,basedim(1)*mf+1])
    set(h,'Box','On','XGrid','On','YGrid','On')
    switch mf
        case 1
            set(h,'FontName','Lucida Sans Unicode','FontSize',12);
        case 2
            set(h,'FontName','Lucida Sans Unicode','FontSize',10);
        otherwise
            set(h,'FontName','Lucida Sans Unicode','FontSize',6);
    end

    global oncolor
%     oncolor = [1,1,0]; % color of engaged circle.
    oncolor = [255, 251, 204] ./ 255;
    
    global offcolor
    offcolor = [0.75 0.75 0.75];

    % now make some circles.
    circleh = zeros(basedim(1)*basedim(2),1);
    circlepos = zeros(basedim(1)*basedim(2),2);
    for l = 1:(basedim(1)*mf)
        for k = 1:(basedim(2)*mf)
            circleh(l+(k-1)*basedim(1)*mf) = rectangle('Position',[k-.5,mf*basedim(1)-(l-.5),1,1],'Curvature',[1,1],'FaceColor',offcolor,'EdgeColor',[.3,.3,.3]);
            circlepos(l+(k-1)*basedim(1)*mf,:) = [k,mf*basedim(1)-l+1];
        end
    end

    % when you click them, do something cool.
    for hh = circleh(:)'
        set(hh,'HitTest','off');
    end
    drawnow;

    % save handles in the figure
    % https://www.youtube.com/watch?v=TV3Oncvz_cU
    set(h,'UserData',{circleh,circlepos});
    % circleh are the handles to each circle
    % circlepos are the positions of each circle (in the same order as circleh)
    % These positions are not relative to the physical position of the strains.
