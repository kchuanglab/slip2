function poslist = genPosList(rown,coln)
    % generate a position list for stage to move    
    global Scp
    mvx = Scp.overlap*Scp.micronsperpixel*Scp.imageWidth;
    mvy = Scp.overlap*Scp.micronsperpixel*Scp.imageHeight;
    posx = mvx * (0 : rown - 1);
    posy = mvy * (0 : coln - 1);
    [poslistx, poslisty] = meshgrid(posx, posy);
    for i = 1 : floor(length(posx) / 2)
        poslisty(:, i * 2) = flipud(poslisty(:, i * 2));
    end
    poslistx = reshape(poslistx, 1, []);
    poslisty = reshape(poslisty, 1, []);
    poslist = [poslistx; poslisty];
    