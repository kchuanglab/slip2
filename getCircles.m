function [circleh,circlepos] = getCircles(handles)

    circledata = get(handles.axes_plate,'UserData');
    circleh = circledata{1};
    circlepos = circledata{2};
