function getPxlSize()
% get pixel size from the system
% update pixel size information
global Scpname
global Scp

curobj = char(Scp.mmc.getProperty('TINosePiece','Label'));
curobj = str2double(curobj(1));

switch Scpname
    case 'colossus'
        if curobj == 1
            Scp.micronsperpixel = 0.0644;
        elseif curobj == 4
            Scp.micronsperpixel = 0.1073;
        else
            Scp.micronsperpixel = str2double(inputdlg('What is microns per pixel of current objective settings?'));
        
        end
    case 'vulcan'
        if curobj == 5
            Scp.micronsperpixel = 0.08;
        else
            Scp.micronsperpixel = str2double(inputdlg('What is microns per pixel of current objective settings?'));

        end
    case 'polaris'
        if curobj == 4
            Scp.micronsperpixel = 0.0644;
        else
            Scp.micronsperpixel = str2double(inputdlg('What is microns per pixel of current objective settings?'));
        end

    otherwise
        error('Scope name not recognized!');
end