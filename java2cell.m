
function cl = java2cell(jv)
    n=jv.size;
    cl = cell(n,1);
    for i=1:n
        cl(i)=jv.get(i-1);
    end
end