function moveStage(posx,posy)
    global Scp
    stagename = Scp.mmc.getXYStageDevice();
    Scp.mmc.setProperty(stagename,'SerialCommand','TTL Y=0');
    Scp.mmc.setXYPosition(stagename,posx,posy);
    scopebusy();
    Scp.mmc.setProperty(stagename,'SerialCommand','TTL Y=2');
    Scp.mmc.setProperty(stagename,'SerialCommand','AHOME');