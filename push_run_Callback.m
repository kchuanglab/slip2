% --- Executes on button press in push_run.
function push_run_Callback(hObject, eventdata, handles)
set(handles.push_stop,'UserData',0) % UserData 1 for stop
% hObject    handle to push_run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Scp
global stop
stop = 0;
global mf
global basedim
global DualCam
global letters
% global wellsep

% update scope image setting
Scp.imageWidth = Scp.mmc.getImageWidth();
Scp.imageHeight = Scp.mmc.getImageHeight();
Scp.bytesPerPixel = Scp.mmc.getBytesPerPixel();
Scp.bitDepth = Scp.mmc.getImageBitDepth();
Scp.micronsperpixel = Scp.mmc.getPixelSizeUm();
% Scp.micronsperpixel = 0.064;
% Get circles
[circleh,circlepos] = getCircles(handles);

strains2image = 0;

global oncolor
for h = circleh(:)'
    if isequal(get(h,'FaceColor'),oncolor)
       strains2image = strains2image+1; 
    end
end

if strains2image == 0
    uiwait(msgbox('Select at least one strain to start acquisition.'));
    return;
end

sendmail = 0;

Scp.gui.closeAllAcquisitions();
Scp.gui.enableLiveMode(false); % disable live after hitting run

% check channels to take
% channel_list = {};
allchannels = get(handles.channel_listbox, 'String');
index_selected = get(handles.channel_listbox, 'Value');
channel_list = allchannels(index_selected);

if length(channel_list) < 1
    uiwait(msgbox('Please select at least one channel for imaging acquisition.'));
    return;
end

if DualCam && ((length(channel_list) ~= 2) || ~(strcmp(channel_list{1}, 'Phase') && strcmp(channel_list{2}, 'EGFP')))
    dualchoice = questdlg('Twin-camera only supports phase + gfp channels. Do you still want to use twin-camera setting?', ...
        'Twin camera option', ...
        'Yes', 'No', 'No');
    switch dualchoice
        case 'Yes'
            uiwait(msgbox('Setting acquisition channels to phase + gfp. Click Run again to start acquisition.'));
            set(handles.channel_listbox, 'value', [1 2]);
%             set(handles.checkbox_phase, 'Value', 1);
%             set(handles.checkbox_gfp, 'Value', 1);
%             set(handles.checkbox_mcherry, 'Value', 0);
            channel_list = {'Phase', 'EGFP'};
            return;
        case 'No'
            DualCam = 0;
            uiwait(msgbox('No longer using twin-camera for acquisition. Click Run again to start acquisition.'));
            return;
    end
end

if DualCam
    isext = 1;
else
    if length(channel_list) == 1
        isext = 1;
    else
        triggerchoice = questdlg('The acquisition is running in a slow mode with two or more channels. Do you want to continue?', ...
        'Acquisition mode', ...
        'Yes', 'No', 'Yes');
        switch triggerchoice
            case 'Yes'
                uiwait(msgbox('Okay, starting acquisition.'));
                isext = 0;
                prompt = channel_list;
                dlg_title = 'Set Exposure';
                num_lines = 1;
                exposure_time = get(handles.edit_exposure,'String');
                defaultans = cell(1, length(channel_list));
                for i = 1 : length(defaultans)
                    defaultans{i} = exposure_time;
                end
                answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
                exp_list = zeros(1, length(answer));
                for i = 1 : length(exp_list)
                    exp_list(i) = str2double(answer{i});
                end
            case 'No'
                uiwait(msgbox('Please select only one channel and restart acquisition.'));
                return;
        end
    end
end

if 0
    isext = 0; %for testing purposes only! remember to change it back!
    channel_list = {'Phase'};
    exp_list = 50;
    uiwait(msgbox('The system is now running in slow mode! Only for testing purposes!'));
end

if isext
    triggerExternal();
    exp_list = str2double(get(handles.edit_exposure,'String'));
end

% getPxlSize();
% pixel size already set at the beginning of this function -- HS 17/09/28

channel_exp_str = [];
if DualCam
    for i = 1 : length(channel_list)
        if ~isempty(channel_exp_str)
            channel_exp_str = [channel_exp_str, '; '];
        end
        channel_exp_str = [channel_exp_str, ' ', channel_list{i}, ': ', ...
            num2str(exp_list), ' ms'];
    end
    channel_exp_str = [channel_exp_str, '; twin camera mode'];
    
    % resetting camera settings
    setDeviceProperty('Multi Camera', 'Physical Camera 1', 'Zyla 2');
    setDeviceProperty('Multi Camera', 'Physical Camera 2', 'Zyla 1');
    Scp.mmc.setCameraDevice('Multi Camera');
    
else
    for i = 1 : length(channel_list)
        if ~isempty(channel_exp_str)
            channel_exp_str = [channel_exp_str, '; '];
        end
        channel_exp_str = [channel_exp_str, ' ', channel_list{i}, ': ', ...
            num2str(exp_list(i)), ' ms'];
    end
end
set(handles.edit9, 'String', ['Channels and exposure time: ' channel_exp_str]);

% Get fov information
[nrows,ncols] = updateFOVgrid(handles);

% Prepare stage for hardware triggered imaging seq
if isext
    prepStage(nrows,ncols);
    pos_list = [];
else
    pos_list = genPosList(nrows, ncols);
end

% Prepare Acquisition 
acqName = get(handles.edit_acq,'String');
Scp.acqName = acqName;
rootDirName = Scp.rootDirName;
numTimePoints = 1;
numFrames = nrows*ncols;
% numPositions = strains2image;

% if strains2image > 1920
%     uiwait(msgbox('SLIP2 supports acquisition of maximum 192 strains each run. If you have more strains, select 192 each time, then reset acquisition and image the remaining. Sorry for the inconvenience.'));
%     return;
% end
% not needed any more -- Handuo Shi 12/02/2015

% time set
timebetween = str2double(get(handles.edit_timeinterval,'String')) * 60; % in seconds
numtime = str2double(get(handles.edit_timepoints,'String'));

numPosMax = 96;
if (strains2image > numPosMax) && (numtime > 1)
    uiwait(msgbox(['Time-lapse not supported for more than ', num2str(numPosMax), ' strains. Setting time points to 1.']));
    set(handles.edit_timepoints, 'String', num2str(1));
    numtime = 1;
end

numAcq = ceil(strains2image / numPosMax);
numPositions = numPosMax * ones(1, numAcq);
numPositions(end) = strains2image - numPosMax * (numAcq - 1);

if get(handles.check_pfsloss,'Value')
    if ~isext
        mailoption = questdlg('Do you want to get notified when PFS is lost?', ...
            'Mail option', ...
            'Yes', 'No', 'Yes');
        switch mailoption
            case 'Yes'
                sendmail = 1;
            case 'No'
                sendmail = 0;
        end
    end
end

mailaddress = [];
if sendmail
    answer = inputdlg('Input your email address:' ,...
        'Email address', ...
        1, {'slipwarning@gmail.com'});
    mailaddress = answer{1};
end

if DualCam
    % reminder to turn on phase and fluor light
    uiwait(msgbox('Please turn on phase light and fluorescence light.'));
end

for acq = 1 : numAcq
    % openAcquisition(java.lang.String name, java.lang.String rootDir, 
    % int nrFrames, int nrChannels, int nrSlices, int nrPositions, boolean show,
    % boolean save)
    if isext
        Scp.gui.openAcquisition(acqName, rootDirName, numTimePoints, DualCam + 1, numFrames, max([numPositions(acq),2]), false, true);
    else
        Scp.gui.openAcquisition(acqName, rootDirName, numTimePoints, length(channel_list), numFrames, max([numPositions(acq),2]), true, true); % if encountering memory issue during acquisition, change the second last "true" to "false".
        for i = 1 : length(channel_list)
            Scp.gui.setChannelName(acqName, i - 1, channel_list(i));
        end
    end
        
    
    % initializeAcquisition(java.lang.String name, int width, int height,
    % int bytesPerPixel, int bitDepth)
    Scp.gui.initializeAcquisition(acqName, Scp.imageWidth, Scp.imageHeight, Scp.bytesPerPixel, Scp.bitDepth);

    for timepos = 1:numtime
        % keep track of start of imaging before every time loop.
        % (for autofocus)
        focusedfirst = 0;
        tic
        
        posIdStart = sum(numPositions(1 : acq)) - numPositions(acq);
        posIdEnd = sum(numPositions(1 : acq));
        curPosId = 0;
        % space loop:
        for platerow = 1:(basedim(1)*mf)
            platecols = 1:(basedim(2)*mf);

            % invert cols for even rows
            if mod(platerow,2)==0
               platecols = platecols(end:-1:1); 
            end

            for platecol = platecols
                % rewrote the image acquisition part into a function -- Handuo
                % Shi 12/01/2015

                if ~stop
                    if isequal(get(circleh(sub2ind(basedim*mf,platerow,platecol)),'FaceColor'),oncolor)
                        curPosId = curPosId + 1;
                        if (curPosId > posIdStart) && (curPosId <= posIdEnd)
                            isfocus = 0;
                            % determine if need autofocus
                            if get(handles.check_autofocusall,'Value')
                                isfocus = 1;
                            elseif ~focusedfirst && get(handles.check_autofocusfirst,'Value')
                                isfocus = 1;
                                focusedfirst = 1;
                            end
                            set(handles.edit_metadata, 'String', ...
                                ['Currently imaging: Well ' letters(platerow) num2str(platecol)]);

                            acqImgCurPos(handles, platecol, platerow, isfocus, timepos, isext, channel_list, pos_list, exp_list, sendmail, mailaddress);
                        end
                    end

%                     Scp.mmc.stopSequenceAcquisition();
                end
            end

        end
        if timepos < numtime
            moveStage(0,0); % prepare for next time step
        end
        timeimaged = toc;
        disp(timeimaged);
        pause(timebetween-timeimaged);
    end
    Scp.gui.closeAcquisition(acqName);
end

triggerInternal();
if ~stop
    uiwait(msgbox('Done!'));
else
    uiwait(msgbox('Acquisition stopped by user!'));
end