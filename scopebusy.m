function scopebusy()
% a wrapper for mmc.waitForSystem, which sometimes crashes.
% a good programmer would recognize that this could easily lead to an
% infinite loop. 
global Scp
try
    Scp.mmc.waitForSystem();
catch
    scopebusy()
end