function setDeviceProperty(devicename,propertyname,propertyvalue)
    global Scp
    propind = [];
    valind = [];
    
    % make comparison easy
    propertyname = lower(propertyname);
    if ischar(propertyvalue)
        propertyvalue = lower(propertyvalue);
    end

    % Get most likely property and value strings:
    allowedprop = java2cell(Scp.mmc.getDevicePropertyNames(devicename));
    propind = find(~cellfun(@isempty,strfind(lower(allowedprop),propertyname)));
    if length(propind)>1
        propind = find(strcmp(lower(allowedprop),propertyname));
    end
    
    if length(propind==1)
        allowedvals = java2cell(Scp.mmc.getAllowedPropertyValues(devicename,allowedprop(propind(1))));
        if isempty(allowedvals)
            allowedvals = {propertyvalue}; % edge case for arbitrary value properties
            valind = 1;
        else
            valind = find(~cellfun(@isempty,strfind(lower(allowedvals),propertyvalue)));
            if length(valind)>1
                valind = find(strcmp(lower(allowedvals),propertyvalue));
            end
        end
    end

    % Set device property to value.
    
    if length(propind==1) && length(valind)==1
        fprintf('Attempting to change Device-Property combination %s-%s\n',devicename,propertyname);
        Scp.mmc.setProperty(devicename,allowedprop{propind},allowedvals{valind});
    else
        if isnumeric(propertyvalue)
            propertyvalue = num2str(propertyvalue);
        end
        error(sprintf('Invalid or Nonexistent Device-Property-Value combination %s-%s-%s\n',devicename,propertyname,propertyvalue));
    end