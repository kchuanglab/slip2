function snapAndAddImage(name, frame, channel, slice, position, posname)
      global Scp
      %try
         if (Scp.mmc.isSequenceRunning())
            ti = Scp.mmc.getLastTaggedImage();
         else
            Scp.mmc.snapImage();
            ti = Scp.mmc.getTaggedImage();
         end
         Scp.mdutils.setChannelIndex(ti.tags, channel);
         Scp.mdutils.setFrameIndex(ti.tags, frame);
         Scp.mdutils.setSliceIndex(ti.tags, slice);

         Scp.mdutils.setPositionIndex(ti.tags, position);

         acq = Scp.gui.getAcquisition(name);
         if (~acq.isInitialized())
            width = Scp.mmc.getImageWidth();
            height = Scp.mmc.getImageHeight();
            depth = Scp.mmc.getBytesPerPixel();
            bitDepth = Scp.mmc.getImageBitDepth();
            multiCamNumCh = Scp.mmc.getNumberOfCameraChannels();

            acq.setImagePhysicalDimensions(width, height, depth, bitDepth, multiCamNumCh);
            acq.initialize();
         end

         if (acq.getPositions() > 1) 
            Scp.mdutils.setPositionName(ti.tags, posname);
         end

         Scp.gui.addImageToAcquisition(name, frame, channel, slice, position, ti);
%       catch
%           error('Cannot acquire images! Error in snapAndAddImage.m. Please try again.');
%       end