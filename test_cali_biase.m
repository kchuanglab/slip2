% calculating the biase from a deviation (x, y, theta) of the actual grid

clear all;
close all;

total_row = 8;
total_col = 12;
D = 9; % 9mm for neighboring wells
R = 3; % radius for each droplet

x = 0;
y = 0;
theta = 0.04;

total_dev = 0;
wells = zeros(total_row, total_col);

for ir = 1 : total_row
    for ic = 1 : total_col
        X1 = (ic - 1) * D;
        Y1 = (ir - 1) * D;
        dis = sqrt((x - X1 * 2 * (sin(theta / 2)) ^ 2 - Y1 * sin(theta)) ^ 2 +...
            (y - Y1 * 2 * (sin(theta / 2)) ^ 2 + X1 * sin(theta)) ^ 2);
        if dis > R
            total_dev = total_dev + 1;
            wells(ir, ic) = 1;
        end
    end
end
imagesc(wells);
axis equal;