%% a script to test position generation
close all; clear all; clc;
%% basic parameters
numstrains = 384; % total number of wells
basedim = [8,12]; % Basic dimensions of plate.
wellsep = 9; % mm (millimeters) between wells in 96-well plate
letters = ['ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'];
%% Check that it is 96-well like
mf = log(numstrains/basedim(1)/basedim(2))/log(2);
if ~mf
    mf = 1;
end
if round(mf)~=mf
    disp('wat')
end
%% figure
figure('Color',[1,1,1]); 
h = gca; hold on;
set(h,'FontName','Arial','FontSize',12);
axis equal;
set(h,'XTick',1:(mf*basedim(2)),'YTick',1:(mf*basedim(1)),'YTickLabel',arrayfun(@(x) x,letters((mf*basedim(1)):-1:1),'UniformOutput',0));
set(h,'Box','On','XGrid','On','YGrid','On')
%% Now plot according to number of wells.

set(h,'XLim',[0,mf*basedim(2)+1],'YLim',[0,mf*basedim(1)+1])

% now make some circles.
radius = wellsep/2;
circleh = zeros(mf*mf*basedim(1)*basedim(2),1);
circlepos = zeros(mf*mf*basedim(1)*basedim(2),2);
for l = 1:(basedim(1)*mf)
    for k = 1:(basedim(2)*mf)
        circleh(l+(k-1)*basedim(1)*mf) = rectangle('Position',[k-.5,mf*basedim(1)-(l-.5),1,1],'Curvature',[1,1],'FaceColor',[1,1,0],'EdgeColor',[.3,.3,.3]);
        circlepos(l+(k-1)*basedim(1)*mf,:) = [k,mf*basedim(1)-l+1];
    end
end
%% Now lets flip them.
for h = circleh(:)'
    set(h,'FaceColor',~get(h,'FaceColor'));
end
%% Now lets selective flip them.
% circlepos = [reshape(meshgrid(1:(multfactor*basedim(2)),1:(multfactor*basedim(1)))',96,1),reshape(meshgrid((multfactor*basedim(1):-1:1),1:(multfactor*basedim(2))),96,1)];
[~,pos] = min(pdist2(ginput(1),circlepos));

set(circleh(pos),'FaceColor',~get(circleh(pos),'FaceColor'));
[a,b] = ind2sub(basedim',pos);
fprintf('You selected Row %c, Column %i.\n',letters(a),b);

%% Now with rectangles!
f = getrect;
f(3:4) = f(1:2)+f(3:4);
tochange = find((circlepos(:,1)>f(1)).*(circlepos(:,1)<f(3)).*(circlepos(:,2)>f(2)).*(circlepos(:,2)<f(4)));
for ll = 1:length(tochange)
    [l,k] = ind2sub(mf*basedim',tochange(ll));
    curcirc = circleh(l+(k-1)*basedim(1)*mf);
    set(curcirc,'FaceColor',~get(curcirc,'FaceColor'));
    fprintf('You selected Row %c, Column %i.\n',letters(l),k);

end
disp('ha')
