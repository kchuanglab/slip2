function togglePosition(curcirc)
global oncolor
global offcolor
curcolor = get(curcirc, 'FaceColor');
if curcolor == oncolor
    set(curcirc, 'FaceColor', offcolor);
else
    set(curcirc, 'FaceColor', oncolor);
end