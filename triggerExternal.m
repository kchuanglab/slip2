function triggerExternal()
global Scp
global Scpname
global DualCam

switch Scpname
    case 'colossus'
        Scp.mmc.setProperty('Neo sCMOS','TriggerMode','External');
    case 'vulcan'
        Scp.mmc.setProperty('Andor','Trigger','External');
        Scp.mmc.setProperty('Andor','FrameTransfer','Off');
    case 'polaris'
        if DualCam
            Scp.mmc.setProperty('Zyla 1', 'TriggerMode', 'External');
        end
        Scp.mmc.setProperty('Zyla 2', 'TriggerMode', 'External');
    otherwise
        error('Scope name not recognized!');
end
