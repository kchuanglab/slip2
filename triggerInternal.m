function triggerInternal()
global Scp
global Scpname
global DualCam

switch Scpname
    case 'colossus'
        Scp.mmc.setProperty('Neo sCMOS','TriggerMode','Internal (Recommended for fast acquisitions)');
    case 'vulcan'
        Scp.mmc.setProperty('Andor','Trigger','Internal');
    case 'polaris'
        if DualCam
            Scp.mmc.setProperty('Zyla 1', 'TriggerMode', 'Internal (Recommended for fast acquisitions)');
        end
        Scp.mmc.setProperty('Zyla 2', 'TriggerMode', 'Internal (Recommended for fast acquisitions)');
    otherwise
        error('Scope name not recognized!');
end