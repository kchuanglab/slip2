function varargout = updateFOVgrid(handles)
    rows = str2double(get(handles.edit_fovrows,'String'));
    cols = str2double(get(handles.edit_fovcols,'String'));
    set(handles.text_fov,'String',[ '= ', num2str(rows*cols), ' fields of view'])
    if nargout == 2
        varargout{1} = rows;
        varargout{2} = cols;
    end